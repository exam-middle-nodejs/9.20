const {Pupil} = require("./pupil");
const { Human } = require("./human");

class Student extends Pupil  {
    constructor (paramName,paramBirthDay,paramCountry,paramSchool,paramGrade,paramPhone,paramMajor,paramStudentId){
        super(paramName,paramBirthDay,paramCountry,paramSchool,paramGrade,paramPhone);
        this.major = paramMajor;
        this.studentId = paramStudentId;
    }
}

var an = new Student("An","10/2","VN","HCMUT","k12","091922","Elec","4420")
console.log(an);
console.log("an thuoc class Pupil: "+Boolean(an instanceof Pupil))
console.log("an thuoc class Human: "+Boolean(an instanceof Human))
