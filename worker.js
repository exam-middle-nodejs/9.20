const Human = require("./human")

class Worker extends Human  {
    constructor(paramName,paramBirthDay,paramCountry,paramJob,paramCompany,paramSalary){
        super(paramName,paramBirthDay,paramCountry);
        this.job = paramJob;
        this.company = paramCompany;
        this.salary = paramSalary
    }
}