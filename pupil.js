const {Human} = require("./human")

class Pupil extends Human  {
    constructor (paramName,paramBirthDay,paramCountry,paramSchool,paramGrade,paramPhone){
        super(paramName,paramBirthDay,paramCountry);
        this.school = paramSchool;
        this.grade = paramGrade;
        this.phone= paramPhone
    }
}
module.exports = {Pupil}
var human1 = new Human("human123","12/1","vn")
var pupil1 = new Pupil("pupil1","10/2","USA","FPT","fresher","090123")
console.log(human1)
console.log(pupil1)
console.log(pupil1.phone)
console.log("human1 is instanceof Pupil: "+Boolean(pupil1 instanceof Pupil))
console.log("human1 is instanceof Human: "+Boolean(pupil1 instanceof Human))

console.log("human1 is instanceof Human: "+Boolean(human1 instanceof Human))